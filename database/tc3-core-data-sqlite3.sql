-- tc3-core-data-sqlite3.sql
-- Copyright © 2019 NextStep IT Training. All rights reserved.
--
-- Core data that should be loaded for all applications. There is one "administrator" user with the password "password".
--

-- Country Codes
 
insert into country_codes (country_code_id, name) values ('AF', 'Afghanistan');
insert into country_codes (country_code_id, name) values ('AL', 'Albania');
insert into country_codes (country_code_id, name) values ('DZ', 'Algeria');
insert into country_codes (country_code_id, name) values ('AS', 'American Samoa');
insert into country_codes (country_code_id, name) values ('AD', 'Andorra');
insert into country_codes (country_code_id, name) values ('AO', 'Angola');
insert into country_codes (country_code_id, name) values ('AI', 'Anguilla');
insert into country_codes (country_code_id, name) values ('AQ', 'Antarctica');
insert into country_codes (country_code_id, name) values ('AG', 'Antigua and Barbuda');
insert into country_codes (country_code_id, name) values ('AR', 'Argentina');
insert into country_codes (country_code_id, name) values ('AM', 'Armenia');
insert into country_codes (country_code_id, name) values ('AW', 'Aruba');
insert into country_codes (country_code_id, name) values ('AU', 'Australia');
insert into country_codes (country_code_id, name) values ('AT', 'Austria');
insert into country_codes (country_code_id, name) values ('AZ', 'Azerbaijan');
insert into country_codes (country_code_id, name) values ('BS', 'Bahamas');
insert into country_codes (country_code_id, name) values ('BH', 'Bahrain');
insert into country_codes (country_code_id, name) values ('BD', 'Bangladesh');
insert into country_codes (country_code_id, name) values ('BB', 'Barbados');
insert into country_codes (country_code_id, name) values ('BY', 'Belarus');
insert into country_codes (country_code_id, name) values ('BE', 'Belgium');
insert into country_codes (country_code_id, name) values ('BZ', 'Belize');
insert into country_codes (country_code_id, name) values ('BJ', 'Benin');
insert into country_codes (country_code_id, name) values ('BM', 'Bermuda');
insert into country_codes (country_code_id, name) values ('BT', 'Bhutan');
insert into country_codes (country_code_id, name) values ('BO', 'Bolivia');
insert into country_codes (country_code_id, name) values ('BQ', 'Bonaire');
insert into country_codes (country_code_id, name) values ('BA', 'Bosnia and Herzegovina');
insert into country_codes (country_code_id, name) values ('BW', 'Botswana');
insert into country_codes (country_code_id, name) values ('BV', 'Bouvet Island');
insert into country_codes (country_code_id, name) values ('BR', 'Brazil');
insert into country_codes (country_code_id, name) values ('IO', 'British Indian Ocean Territory');
insert into country_codes (country_code_id, name) values ('BN', 'Brunei Darussalam');
insert into country_codes (country_code_id, name) values ('BG', 'Bulgaria');
insert into country_codes (country_code_id, name) values ('BF', 'Burkina Faso');
insert into country_codes (country_code_id, name) values ('BI', 'Burundi');
insert into country_codes (country_code_id, name) values ('KH', 'Cambodia');
insert into country_codes (country_code_id, name) values ('CM', 'Cameroon');
insert into country_codes (country_code_id, name) values ('CA', 'Canada');
insert into country_codes (country_code_id, name) values ('CV', 'Cape Verde');
insert into country_codes (country_code_id, name) values ('KY', 'Cayman Islands');
insert into country_codes (country_code_id, name) values ('CF', 'Central African Republic');
insert into country_codes (country_code_id, name) values ('TD', 'Chad');
insert into country_codes (country_code_id, name) values ('CL', 'Chile');
insert into country_codes (country_code_id, name) values ('CN', 'China');
insert into country_codes (country_code_id, name) values ('CX', 'Christmas Island');
insert into country_codes (country_code_id, name) values ('CC', 'Cocos (Keeling) Islands');
insert into country_codes (country_code_id, name) values ('CO', 'Colombia');
insert into country_codes (country_code_id, name) values ('KM', 'Comoros');
insert into country_codes (country_code_id, name) values ('CG', 'Congo');
insert into country_codes (country_code_id, name) values ('CD', 'Democratic Republic of the Congo');
insert into country_codes (country_code_id, name) values ('CK', 'Cook Islands');
insert into country_codes (country_code_id, name) values ('CR', 'Costa Rica');
insert into country_codes (country_code_id, name) values ('HR', 'Croatia');
insert into country_codes (country_code_id, name) values ('CU', 'Cuba');
insert into country_codes (country_code_id, name) values ('CW', 'Curacao');
insert into country_codes (country_code_id, name) values ('CY', 'Cyprus');
insert into country_codes (country_code_id, name) values ('CZ', 'Czech Republic');
insert into country_codes (country_code_id, name) values ('CI', 'Cote d''Ivoire');
insert into country_codes (country_code_id, name) values ('DK', 'Denmark');
insert into country_codes (country_code_id, name) values ('DJ', 'Djibouti');
insert into country_codes (country_code_id, name) values ('DM', 'Dominica');
insert into country_codes (country_code_id, name) values ('DO', 'Dominican Republic');
insert into country_codes (country_code_id, name) values ('EC', 'Ecuador');
insert into country_codes (country_code_id, name) values ('EG', 'Egypt');
insert into country_codes (country_code_id, name) values ('SV', 'El Salvador');
insert into country_codes (country_code_id, name) values ('GQ', 'Equatorial Guinea');
insert into country_codes (country_code_id, name) values ('ER', 'Eritrea');
insert into country_codes (country_code_id, name) values ('EE', 'Estonia');
insert into country_codes (country_code_id, name) values ('ET', 'Ethiopia');
insert into country_codes (country_code_id, name) values ('FK', 'Falkland Islands (Malvinas)');
insert into country_codes (country_code_id, name) values ('FO', 'Faroe Islands');
insert into country_codes (country_code_id, name) values ('FJ', 'Fiji');
insert into country_codes (country_code_id, name) values ('FI', 'Finland');
insert into country_codes (country_code_id, name) values ('FR', 'France');
insert into country_codes (country_code_id, name) values ('GF', 'French Guiana');
insert into country_codes (country_code_id, name) values ('PF', 'French Polynesia');
insert into country_codes (country_code_id, name) values ('TF', 'French Southern Territories');
insert into country_codes (country_code_id, name) values ('GA', 'Gabon');
insert into country_codes (country_code_id, name) values ('GM', 'Gambia');
insert into country_codes (country_code_id, name) values ('GE', 'Georgia');
insert into country_codes (country_code_id, name) values ('DE', 'Germany');
insert into country_codes (country_code_id, name) values ('GH', 'Ghana');
insert into country_codes (country_code_id, name) values ('GI', 'Gibraltar');
insert into country_codes (country_code_id, name) values ('GR', 'Greece');
insert into country_codes (country_code_id, name) values ('GL', 'Greenland');
insert into country_codes (country_code_id, name) values ('GD', 'Grenada');
insert into country_codes (country_code_id, name) values ('GP', 'Guadeloupe');
insert into country_codes (country_code_id, name) values ('GU', 'Guam');
insert into country_codes (country_code_id, name) values ('GT', 'Guatemala');
insert into country_codes (country_code_id, name) values ('GG', 'Guernsey');
insert into country_codes (country_code_id, name) values ('GN', 'Guinea');
insert into country_codes (country_code_id, name) values ('GW', 'Guinea-Bissau');
insert into country_codes (country_code_id, name) values ('GY', 'Guyana');
insert into country_codes (country_code_id, name) values ('HT', 'Haiti');
insert into country_codes (country_code_id, name) values ('HM', 'Heard Island and McDonald Islands');
insert into country_codes (country_code_id, name) values ('VA', 'Holy See (Vatican City State)');
insert into country_codes (country_code_id, name) values ('HN', 'Honduras');
insert into country_codes (country_code_id, name) values ('HK', 'Hong Kong');
insert into country_codes (country_code_id, name) values ('HU', 'Hungary');
insert into country_codes (country_code_id, name) values ('IS', 'Iceland');
insert into country_codes (country_code_id, name) values ('IN', 'India');
insert into country_codes (country_code_id, name) values ('ID', 'Indonesia');
insert into country_codes (country_code_id, name) values ('IR', 'Iran Islamic Republic of');
insert into country_codes (country_code_id, name) values ('IQ', 'Iraq');
insert into country_codes (country_code_id, name) values ('IE', 'Ireland');
insert into country_codes (country_code_id, name) values ('IM', 'Isle of Man');
insert into country_codes (country_code_id, name) values ('IL', 'Israel');
insert into country_codes (country_code_id, name) values ('IT', 'Italy');
insert into country_codes (country_code_id, name) values ('JM', 'Jamaica');
insert into country_codes (country_code_id, name) values ('JP', 'Japan');
insert into country_codes (country_code_id, name) values ('JE', 'Jersey');
insert into country_codes (country_code_id, name) values ('JO', 'Jordan');
insert into country_codes (country_code_id, name) values ('KZ', 'Kazakhstan');
insert into country_codes (country_code_id, name) values ('KE', 'Kenya');
insert into country_codes (country_code_id, name) values ('KI', 'Kiribati');
insert into country_codes (country_code_id, name) values ('KP', 'Korea Democratic People''s Republic of');
insert into country_codes (country_code_id, name) values ('KR', 'Korea Republic of');
insert into country_codes (country_code_id, name) values ('KW', 'Kuwait');
insert into country_codes (country_code_id, name) values ('KG', 'Kyrgyzstan');
insert into country_codes (country_code_id, name) values ('LA', 'Lao People''s Democratic Republic');
insert into country_codes (country_code_id, name) values ('LV', 'Latvia');
insert into country_codes (country_code_id, name) values ('LB', 'Lebanon');
insert into country_codes (country_code_id, name) values ('LS', 'Lesotho');
insert into country_codes (country_code_id, name) values ('LR', 'Liberia');
insert into country_codes (country_code_id, name) values ('LY', 'Libya');
insert into country_codes (country_code_id, name) values ('LI', 'Liechtenstein');
insert into country_codes (country_code_id, name) values ('LT', 'Lithuania');
insert into country_codes (country_code_id, name) values ('LU', 'Luxembourg');
insert into country_codes (country_code_id, name) values ('MO', 'Macao');
insert into country_codes (country_code_id, name) values ('MK', 'Macedonia the Former Yugoslav Republic of');
insert into country_codes (country_code_id, name) values ('MG', 'Madagascar');
insert into country_codes (country_code_id, name) values ('MW', 'Malawi');
insert into country_codes (country_code_id, name) values ('MY', 'Malaysia');
insert into country_codes (country_code_id, name) values ('MV', 'Maldives');
insert into country_codes (country_code_id, name) values ('ML', 'Mali');
insert into country_codes (country_code_id, name) values ('MT', 'Malta');
insert into country_codes (country_code_id, name) values ('MH', 'Marshall Islands');
insert into country_codes (country_code_id, name) values ('MQ', 'Martinique');
insert into country_codes (country_code_id, name) values ('MR', 'Mauritania');
insert into country_codes (country_code_id, name) values ('MU', 'Mauritius');
insert into country_codes (country_code_id, name) values ('YT', 'Mayotte');
insert into country_codes (country_code_id, name) values ('MX', 'Mexico');
insert into country_codes (country_code_id, name) values ('F', 'Micronesia Federated States of');
insert into country_codes (country_code_id, name) values ('M', 'Moldova Republic of');
insert into country_codes (country_code_id, name) values ('MC', 'Monaco');
insert into country_codes (country_code_id, name) values ('MN', 'Mongolia');
insert into country_codes (country_code_id, name) values ('ME', 'Montenegro');
insert into country_codes (country_code_id, name) values ('MS', 'Montserrat');
insert into country_codes (country_code_id, name) values ('MA', 'Morocco');
insert into country_codes (country_code_id, name) values ('MZ', 'Mozambique');
insert into country_codes (country_code_id, name) values ('MM', 'Myanmar');
insert into country_codes (country_code_id, name) values ('NA', 'Namibia');
insert into country_codes (country_code_id, name) values ('NR', 'Nauru');
insert into country_codes (country_code_id, name) values ('NP', 'Nepal');
insert into country_codes (country_code_id, name) values ('NL', 'Netherlands');
insert into country_codes (country_code_id, name) values ('NC', 'New Caledonia');
insert into country_codes (country_code_id, name) values ('NZ', 'New Zealand');
insert into country_codes (country_code_id, name) values ('NI', 'Nicaragua');
insert into country_codes (country_code_id, name) values ('NE', 'Niger');
insert into country_codes (country_code_id, name) values ('NG', 'Nigeria');
insert into country_codes (country_code_id, name) values ('NU', 'Niue');
insert into country_codes (country_code_id, name) values ('NF', 'Norfolk Island');
insert into country_codes (country_code_id, name) values ('MP', 'Northern Mariana Islands');
insert into country_codes (country_code_id, name) values ('NO', 'Norway');
insert into country_codes (country_code_id, name) values ('OM', 'Oman');
insert into country_codes (country_code_id, name) values ('PK', 'Pakistan');
insert into country_codes (country_code_id, name) values ('PW', 'Palau');
insert into country_codes (country_code_id, name) values ('P', 'Palestine State of');
insert into country_codes (country_code_id, name) values ('PA', 'Panama');
insert into country_codes (country_code_id, name) values ('PG', 'Papua New Guinea');
insert into country_codes (country_code_id, name) values ('PY', 'Paraguay');
insert into country_codes (country_code_id, name) values ('PE', 'Peru');
insert into country_codes (country_code_id, name) values ('PH', 'Philippines');
insert into country_codes (country_code_id, name) values ('PN', 'Pitcairn');
insert into country_codes (country_code_id, name) values ('PL', 'Poland');
insert into country_codes (country_code_id, name) values ('PT', 'Portugal');
insert into country_codes (country_code_id, name) values ('PR', 'Puerto Rico');
insert into country_codes (country_code_id, name) values ('QA', 'Qatar');
insert into country_codes (country_code_id, name) values ('RO', 'Romania');
insert into country_codes (country_code_id, name) values ('RU', 'Russian Federation');
insert into country_codes (country_code_id, name) values ('RW', 'Rwanda');
insert into country_codes (country_code_id, name) values ('RE', 'Reunion');
insert into country_codes (country_code_id, name) values ('BL', 'Saint Barthelemy');
insert into country_codes (country_code_id, name) values ('SH', 'Saint Helena');
insert into country_codes (country_code_id, name) values ('KN', 'Saint Kitts and Nevis');
insert into country_codes (country_code_id, name) values ('LC', 'Saint Lucia');
insert into country_codes (country_code_id, name) values ('MF', 'Saint Martin (French part)');
insert into country_codes (country_code_id, name) values ('PM', 'Saint Pierre and Miquelon');
insert into country_codes (country_code_id, name) values ('VC', 'Saint Vincent and the Grenadines');
insert into country_codes (country_code_id, name) values ('WS', 'Samoa');
insert into country_codes (country_code_id, name) values ('SM', 'San Marino');
insert into country_codes (country_code_id, name) values ('ST', 'Sao Tome and Principe');
insert into country_codes (country_code_id, name) values ('SA', 'Saudi Arabia');
insert into country_codes (country_code_id, name) values ('SN', 'Senegal');
insert into country_codes (country_code_id, name) values ('RS', 'Serbia');
insert into country_codes (country_code_id, name) values ('SC', 'Seychelles');
insert into country_codes (country_code_id, name) values ('SL', 'Sierra Leone');
insert into country_codes (country_code_id, name) values ('SG', 'Singapore');
insert into country_codes (country_code_id, name) values ('SX', 'Sint Maarten (Dutch part)');
insert into country_codes (country_code_id, name) values ('SK', 'Slovakia');
insert into country_codes (country_code_id, name) values ('SI', 'Slovenia');
insert into country_codes (country_code_id, name) values ('SB', 'Solomon Islands');
insert into country_codes (country_code_id, name) values ('SO', 'Somalia');
insert into country_codes (country_code_id, name) values ('ZA', 'South Africa');
insert into country_codes (country_code_id, name) values ('GS', 'South Georgia and the South Sandwich Islands');
insert into country_codes (country_code_id, name) values ('SS', 'South Sudan');
insert into country_codes (country_code_id, name) values ('ES', 'Spain');
insert into country_codes (country_code_id, name) values ('LK', 'Sri Lanka');
insert into country_codes (country_code_id, name) values ('SD', 'Sudan');
insert into country_codes (country_code_id, name) values ('SR', 'Suriname');
insert into country_codes (country_code_id, name) values ('SJ', 'Svalbard and Jan Mayen');
insert into country_codes (country_code_id, name) values ('SZ', 'Swaziland');
insert into country_codes (country_code_id, name) values ('SE', 'Sweden');
insert into country_codes (country_code_id, name) values ('CH', 'Switzerland');
insert into country_codes (country_code_id, name) values ('SY', 'Syrian Arab Republic');
insert into country_codes (country_code_id, name) values ('TW', 'Taiwan');
insert into country_codes (country_code_id, name) values ('TJ', 'Tajikistan');
insert into country_codes (country_code_id, name) values ('TZ', 'United Republic of Tanzania');
insert into country_codes (country_code_id, name) values ('TH', 'Thailand');
insert into country_codes (country_code_id, name) values ('TL', 'Timor-Leste');
insert into country_codes (country_code_id, name) values ('TG', 'Togo');
insert into country_codes (country_code_id, name) values ('TK', 'Tokelau');
insert into country_codes (country_code_id, name) values ('TO', 'Tonga');
insert into country_codes (country_code_id, name) values ('TT', 'Trinidad and Tobago');
insert into country_codes (country_code_id, name) values ('TN', 'Tunisia');
insert into country_codes (country_code_id, name) values ('TR', 'Turkey');
insert into country_codes (country_code_id, name) values ('TM', 'Turkmenistan');
insert into country_codes (country_code_id, name) values ('TC', 'Turks and Caicos Islands');
insert into country_codes (country_code_id, name) values ('TV', 'Tuvalu');
insert into country_codes (country_code_id, name) values ('UG', 'Uganda');
insert into country_codes (country_code_id, name) values ('UA', 'Ukraine');
insert into country_codes (country_code_id, name) values ('AE', 'United Arab Emirates');
insert into country_codes (country_code_id, name) values ('GB', 'United Kingdom');
insert into country_codes (country_code_id, name) values ('US', 'United States');
insert into country_codes (country_code_id, name) values ('UM', 'United States Minor Outlying Islands');
insert into country_codes (country_code_id, name) values ('UY', 'Uruguay');
insert into country_codes (country_code_id, name) values ('UZ', 'Uzbekistan');
insert into country_codes (country_code_id, name) values ('VU', 'Vanuatu');
insert into country_codes (country_code_id, name) values ('VE', 'Venezuela');
insert into country_codes (country_code_id, name) values ('VN', 'Viet Nam');
insert into country_codes (country_code_id, name) values ('VG', 'British Virgin Islands');
insert into country_codes (country_code_id, name) values ('VI', 'US Virgin Islands');
insert into country_codes (country_code_id, name) values ('WF', 'Wallis and Futuna');
insert into country_codes (country_code_id, name) values ('EH', 'Western Sahara');
insert into country_codes (country_code_id, name) values ('YE', 'Yemen');
insert into country_codes (country_code_id, name) values ('ZM', 'Zambia');
insert into country_codes (country_code_id, name) values ('ZW', 'Zimbabwe');

-- Authorities

insert into authorities (name) values ('owner');
insert into authorities (name) values ('cashier');
insert into authorities (name) values ('customer service');
insert into authorities (name) values ('administrator');

-- Customers

insert into customers (email, password, first_name, last_name, street, city, state_or_province, postal_code, country_code_id, birthdate, confirmation_code, rewards, authority_id) values ('administrator@tc3.com', 'password', 'System', 'Administrator', null, null, null, null, 'US', null, null, 0, 4);

-- Customer Cards

-- Payment Types

insert into payment_types (payment_type_id, name) values (1, 'Cash');
insert into payment_types (payment_type_id, name) values (2, 'Card');
insert into payment_types (payment_type_id, name) values (3, 'Reward');
insert into payment_types (payment_type_id, name) values (4, 'Credit');

-- Product Types

insert into product_types (product_type_id, name) values (1, 'Pastry');
insert into product_types (product_type_id, name) values (2, 'Beverage');

-- Products

-- Sales Orders

-- Sales Order Items

-- Rewards
