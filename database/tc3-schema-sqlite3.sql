-- tc3-schema-sqlite3.sql
-- Copyright © 2019 NextStep IT Training. All rights reserved.
--
-- The is the database schema used for creating the in-memory database for integration tests.
--

drop table if exists rewards;
drop table if exists sales_order_items;
drop table if exists sales_orders;
drop table if exists products;
drop table if exists product_types;
drop table if exists payment_types;
drop table if exists customer_cards;
drop table if exists customers;
drop table if exists authorities;
drop table if exists country_codes;

create table country_codes (
    country_code_id varchar(3) primary key,
    name varchar(255) not null
);

-- Authorities are fixed to owner (1), cashier (2), customer service (3), and administrator (4); the authorities
-- are cumulative and each has all of the rights of the authorities lower than it.

create table authorities (
    authority_id integer primary key autoincrement,
    name varchar(20) not null
);

create table customers (
    customer_id integer primary key autoincrement,
    email varchar(128) not null,
    password varchar(128) not null,
    first_name varchar(40) not null,
    last_name varchar(40) not null,
    street varchar(40),
    city varchar(40),
    state_or_province varchar(40),
    postal_code varchar(20),
    country_code_id varchar(3),
    birthdate date,
    confirmation_code varchar(128),
    rewards integer,
    authority_id integer,
    constraint fk_customers_country_codes foreign key(country_code_id) references country_codes(country_code_id),
    constraint fk_customers_authorities foreign key(authority_id) references authorities(authority_id)
);

create table customer_cards (
    customer_card_id integer primary key autoincrement,
    customer_id integer not null,
    preferred boolean not null,
    card_number varchar(20) not null,
    expires date not null,
    ccv integer,
    constraint fk_customer_cards_customers foreign key(customer_id) references customers(customer_id)
);

-- Payment types will be fixed to cash (1), card (2), reward (3), and credit (4)

create table payment_types (
    payment_type_id integer primary key autoincrement,
    name varchar(20) not null
);

create table product_types (
    product_type_id integer primary key autoincrement,
    name varchar(20) not null
);

create table products (
    product_id integer primary key autoincrement,
    product_type_id integer not null,
    name varchar(255) not null,
    price decimal(10, 2) not null,
    constraint fk_products_product_types foreign key(product_type_id) references product_types(product_type_id)
);

create table product_images (
    product_id integer references products (product_id) not null,
    small_image boolean not null,
    image blob,
    primary_key (
        product_id,
        small_image
    )
);

create table sales_orders (
    sales_order_id integer primary key autoincrement,
    order_date datetime not null,
    customer_id integer not null,
    total decimal(10, 2),
    payment_type_id integer,
    card_number varchar(20),
    card_expires date,
    filled datetime,
    constraint fk_sales_orders_customers foreign key(customer_id) references customers(customer_id),
    constraint fk_sales_orders_payment_types foreign key(payment_type_id) references payment_types(payment_type_id)
);

create table sales_order_items (
    sales_order_item_id integer primary key autoincrement,
    sales_order_id integer not null,
    product_id integer not null,
    quantity integer not null,
    price decimal(10, 2) not null,
    tax decimal(10, 2) not null,
    constraint fk_sales_order_items_sales_orders foreign key(sales_order_id) references sales_orders(sales_order_id),
    constraint fk_sales_order_items_products foreign key(product_id) references products(product_id)
);

create table rewards
(
    reward_id integer primary key autoincrement,
    customer_id integer not null,
    sales_order_id integer,
    recorded date not null,
    amount decimal(10, 2) not null,
    description varchar(255),
    constraint fk_rewards_customers foreign key (customer_id) references customers (customer_id),
    constraint fk_rewards_sales_orders foreign key (sales_order_id) references sales_orders (sales_order_id)
);
