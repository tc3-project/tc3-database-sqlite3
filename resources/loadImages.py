# loadImages.py
# Copyright © 2020 Joel Mussman. All rights reserved.
#
# The images are already in the database; this module is left in place in case the database needs
# to be recreated from the schema, data, and images.
#
# This python program populates the product_images table from the large and small image files
# that are indexed by their product id. Run this program in the resources folder, it uses
# relative paths for the database and image files.
#

from os import walk
import re
import sqlite3

def loadBinaryData(filename):
    with open(filename, 'rb') as file:
        blobData = file.read()
    return blobData

def openDatabase(filename):
    sqliteConnection = sqlite3.connect('../database/tc3.db')
    cursor = sqliteConnection.cursor()
    print("Connected to SQLite")
    return (sqliteConnection, cursor)

def closeDatabase(sqliteConnection):
    if sqliteConnection:
        sqliteConnection.commit()
        sqliteConnection.close()
    print("Closed sqlite3 database")

def insertBLOB(cursor, product_id, fileData, small_image):
    sqlite_insert_blob_query = """ INSERT INTO product_images
                              (product_id, image, small_image) VALUES (?, ?, ?)"""
    cursor.execute(sqlite_insert_blob_query, (product_id, fileData, small_image))
    print("Image and file inserted successfully as a BLOB into a table")

def loadImages(folder, small_image, cursor):
    for (dirpath, dirnames, filenames) in walk(folder):
        for filename in filenames:
            product_id = re.search('\d+', filename)[0]
            print("Inserting", "small" if small_image else "large", "image for", product_id, "from", folder + filename)
            insertBLOB(cursor, product_id, loadBinaryData(folder + filename), small_image)

def main():
    try:
        (sqliteConnection, cursor) = openDatabase("../database/tc3.db")
        loadImages("./images by product_id/small-images/", True, cursor)
        loadImages("./images by product_id/large-images/", False, cursor)

    except sqlite3.Error as error:
        print(error)

    finally:
        closeDatabase(sqliteConnection)

main()