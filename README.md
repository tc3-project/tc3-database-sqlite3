# The Caribbean Coffee Company ![](./.common/logo.png?raw=true)
by Joel Mussman<br/>
original project concept by Joel Mussman and Justin Poole 

The Caribbean Coffee Company is a cafe-operations project that I have developed as the basis for doing demonstrations
and class labs for almost every software development technology that I work with, from databases through devices.
The premise is that a customer can use a web site or a device app to browse the menu of the cafe, place an order,
and pay for it.
Cashiers and managers also reach the back-end through the web portal and point-of-sale terminals.

## TC3-Database-SQLite3

This is the SQLite3 version of the TC3 database as SQL scripts and a sample SQLite3 database built from the scripts.

This database schema and sample data is distributed freely.
If you want to use it for demonstrations or as the basis of a lab for your own course, feel free.
We consider it to be a compliment and an honor that you want to use our material, and we would much rather be
collaborators than of competitors!
Feel free to contact us, and if you could, please give credit when you use this.

Updated 7/18/2020: new products, small and large images for the products inserted into product_images and saved under
the resources folder.

## License

This project code is licensed under the [MIT license](./.common/LICENSE.md).

## Support

Since I give stuff away for free, and if you would like to keep seeing more stuff like this, then please consider
a contribution to *Joel's Coffee Fund* at **Smallrock Internet** to help keep the good stuff coming :)<br />

[![Donate](./.common/Donate-Paypal.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XPUGVGZZ8RUAA)

## Project Setup

The *database* folder contains an SQL schema file to create the SQLite3 database and an SQL data file to populate the database with sample data.
There is also a *tc3.db* file which contains the SQLite3 database already created and populated.

I recommend using SQLiteStudio ([https://sqlitestudio.pl](https://sqlitestudio.pl)) to explore the database.
There are plenty of other products out there, including SQLite from [https://sqlite.org](https://sqlite.org) itself, for you to choose from.

## Project Details

This is part of of the larger Caribbean Coffee Company suite of components using different technologies and programming languages that fit into the TC3 project.
Learn more at [https://gitlab.com/tc3-project](https://gitlab.com/tc3-project).

<hr>
Copyright © 2019-2020 Joel A Mussman. All rights reserved.